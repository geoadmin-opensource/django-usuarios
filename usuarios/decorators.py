# coding: utf-8
from django.contrib.auth.decorators import user_passes_test
from django.core.exceptions import PermissionDenied


def group_required(*group_names):
    """Requires user membership in at least one of the groups passed in."""

    if len(group_names) == 1 and isinstance(group_names[0], list):
        group_names = [g for g in group_names[0]]

    def in_groups(u):

        if u.is_authenticated():
            if u.is_superuser | bool(u.groups.filter(name__in=group_names)):
                return True

        raise PermissionDenied

    return user_passes_test(in_groups)
