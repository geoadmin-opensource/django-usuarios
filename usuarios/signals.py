# coding: utf-8
from django.dispatch import Signal

signal_excluir_usuarios_usuario = Signal(providing_args=["usuario", "autor"])
signal_excluir_usuarios_grupo = Signal(providing_args=["grupo", "autor"])
signal_editar_usuarios = Signal(providing_args=["usuario", "verb", "autor"])
signal_restaurar_usuarios = Signal(providing_args=["usuario", "autor"])
signal_usuario_add_grupo_usuarios = Signal(providing_args=["grupo", "usuario", "autor"])
signal_excluir_permissao_usuarios = Signal(providing_args=["grupo", "permissao", "autor"])
signal_add_permissao_usuarios = Signal(providing_args=["grupo", "permissao", "autor"])
signal_usuario_excluir_grupo_usuarios = Signal(providing_args=["grupo", "usuario", "autor"])
