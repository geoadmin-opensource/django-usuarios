# coding: utf-8
import re
from django.contrib.auth.models import Permission, PermissionsMixin, AbstractBaseUser, UserManager, BaseUserManager
from django.contrib.gis.db import models
from django.core import validators
from django.core.mail import send_mail
from django.utils.translation import ugettext as _


class UsuarioManager(BaseUserManager):

    def create_user(self, nome_usuario, email, staff, superuser, password=None):

        if not nome_usuario:
            raise ValueError(_(u"Nome usuário é obrigatório."))

        email = self.normalize_email(email)

        user = self.model(nome_usuario=nome_usuario,
                          email=email,
                          is_staff=staff,
                          is_superuser=superuser)

        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, nome_usuario, email, password=None):

        user = self.create_user(nome_usuario, email, True, True, password)
        permissions = Permission.objects.all()
        for perm in permissions:
            user.user_permissions.add(perm)
        user.save(using=self._db)


class UsuarioBase(AbstractBaseUser, PermissionsMixin):

    data_hora_criacao = models.DateTimeField(verbose_name=_(u"Data/Hora Criação"),
                                             help_text=_(u"Data Hora da criação do usuário"),
                                             auto_now_add=True)

    nome_usuario = models.CharField(verbose_name=_(u"Nome de Usuário"),
                                    help_text=_(u"Nome de usuário. Deve ser único. Aceita letras, números, @/./+/-/"),
                                    validators=[validators.RegexValidator(re.compile('^[\w.@+-]+$'),
                                                                          _(u"Nome inválido."),
                                                                          _(u"inválido"))],
                                    max_length=30,
                                    unique=True,)

    nome_completo = models.CharField(verbose_name=_(u"Nome completo"),
                                     help_text=_(u"Nome completo de usuário"),
                                     max_length=128,
                                     null=True,
                                     blank=True,)

    email = models.EmailField(verbose_name=_(u"Email"),
                              help_text=_(u"Email do usuário"),
                              max_length=128,
                              unique=True,)

    is_staff = models.BooleanField(verbose_name=_(u"É staff?"),
                                   help_text=_(u"Indica se o usuário faz parte do staff."),
                                   default=False)

    is_active = models.BooleanField(verbose_name=_(u"Ativo?"),
                                    help_text=_(u"Indica se o usuário está ativo."),
                                    default=True)

    ultimo_login = models.DateTimeField(verbose_name=_(u"Ultimo login"),
                                        help_text=_(u"Indica a data e hora que este usuario realizou o ultimo login no sistema."),
                                        editable=False,
                                        null=True,
                                        blank=True)

    objects = UsuarioManager()

    USERNAME_FIELD = "nome_usuario"
    REQUIRED_FIELDS = ["email"]

    def __unicode__(self):

        return self.nome_usuario

    def get_full_name(self):

        return self.nome_completo

    def get_short_name(self):

        return self.nome_usuario

    def email_user(self, subject, message, from_email=None, html_message=None):

        send_mail(subject,
                  message,
                  from_email,
                  [self.email],
                  html_message=html_message)

    class Meta:

        verbose_name = _(u"Usuário")
        verbose_name_plural = _(u"Usuários")
        ordering = ["nome_usuario", ]
        abstract = True


def unicode_permission(self):
    return self.name

Permission.__unicode__ = unicode_permission
