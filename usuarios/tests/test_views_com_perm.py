# coding: utf-8
from django.test import TestCase, TransactionTestCase
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission, Group
from django.contrib.auth.tokens import default_token_generator
from django.core.urlresolvers import reverse
from django.db.models.loading import get_model
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import get_object_or_404
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from ..views import check_perm

User = get_user_model()


class PermissionsTestCase(TransactionTestCase):
    """ Case de testes relacionados às permissões das views """

    def create_user(self, nome_usuario=None, email=None,
                    nome_completo=None, is_active=True,
                    is_staff=False, is_superuser=False):
        """ Cria usuário no banco """
        try:
            user = User.objects.create(username=nome_usuario or 'footeste',
                                       first_name=nome_completo[:nome_completo.find(' ')],
                                       last_name=nome_completo[nome_completo.find(' '):].split())
        except (TypeError, AttributeError):
            user = User.objects.create(nome_usuario=nome_usuario or 'footeste',
                                       nome_completo=nome_completo)

        user.email = email or 'foo@teste'
        user.is_active = is_active
        user.is_staff = is_staff
        user.is_superuser = is_superuser
        user.set_password('footeste')
        user.save()
        return user

    def create_group(self, name):
        """ Cria um grupo no banco """
        try:
            g = Group.objects.get(name=name)
        except Group.DoesNotExist:
            g = Group.objects.create(name=name)

        return g

    def set_perm(self, perm_name, user_id=None, group_id=None):
        """ Atribui permissão à usuário """
        perm = Permission.objects.get(codename=perm_name)

        if user_id:
            user = User.objects.get(pk=user_id)
            user.user_permissions.add(perm)
            user.save()
            return user

        if group_id:
            group = Group.objects.get(pk=group_id)
            group.permissions.add(perm)
            group.save()
            return group

    def login(self, username=None):
        """ Loga o usuário com parâmetros padrão """
        return self.client.login(username=username or 'footeste', password='footeste')


class CheckPermTestCase(PermissionsTestCase):
    '''Case de testes da função check_perm'''

    def setUp(self):
        self.user = self.create_user()

    def test_check_perm_return_false(self):
        '''Testa se a função retorna false'''
        perms = Permission.objects.all()
        for perm in perms:
            self.assertFalse(check_perm(usuario=self.user, permissoes=perm.codename))
        self.assertFalse(check_perm(None, 'foo'))
        self.assertFalse(check_perm(self.user, None))

    def test_check_perm_return_true(self):
        '''Testa se a função retorna true'''
        perm = Permission.objects.first()

        # permissão associada a usuário
        self.user = self.set_perm(perm_name=perm.codename, user_id=self.user.id)
        self.assertTrue(check_perm(usuario=self.user, permissoes=perm.codename))

        # permissão associada a grupo
        grupo = self.create_group('bar')
        grupo.permissions.add(perm)
        temp = self.create_user(nome_usuario='temp', email='temp@foo.com')
        grupo.user_set.add(temp)
        grupo.save()
        self.assertTrue(check_perm(usuario=temp, permissoes=perm.codename))


class AcessoAnonimoTestCase(TestCase):
    """ Case de testes anônimos das views """

    def test_acesso_anonimo_lista_usuarios(self):
        """Testa o acesso anônimo à view lista_usuarios"""
        url = reverse('usuarios:lista_usuarios')
        response = self.client.get(path=url)
        url = settings.LOGIN_URL + '?next=' + url
        self.assertRedirects(response, url)

    def test_acesso_anonimo_editar_usuario_sistema(self):
        """Testa o acesso anônimo à view editar_usuario_sistema"""
        url = reverse('usuarios:editar_usuario_sistema', args=[1])
        response = self.client.get(path=url)
        url = settings.LOGIN_URL + '?next=' + url
        self.assertRedirects(response, url)

    def test_acesso_anonimo_criar_usuario_sistema(self):
        """Testa o acesso anônimo à view criar_usuario_sistema"""
        url = reverse('usuarios:criar_usuario_sistema')
        response = self.client.get(path=url)
        url = settings.LOGIN_URL + '?next=' + url
        self.assertRedirects(response, url)

    def test_acesso_anonimo_detalhe_usuario(self):
        """Testa o acesso anônimo à view detalhe_usuario"""
        url = reverse('usuarios:detalhe_usuario', args=[1])
        response = self.client.get(path=url)
        url = settings.LOGIN_URL + '?next=' + url
        self.assertRedirects(response, url)

    def test_acesso_anonimo_busca_usuarios_json(self):
        """Testa o acesso anônimo à view busca_usuarios_json"""
        url = reverse('usuarios:busca_usuarios_json')
        response = self.client.get(path=url)
        url = settings.LOGIN_URL + '?next=' + url
        self.assertRedirects(response, url)

    def test_acesso_anonimo_restaura_usuario_sistema(self):
        """Testa o acesso anônimo à view restaura_usuario_sistema"""
        url = reverse('usuarios:restaura_usuario', args=[1])
        response = self.client.get(path=url)
        url = settings.LOGIN_URL + '?next=' + url
        self.assertRedirects(response, url)

    def test_acesso_anonimo_confirm_exclui_usuario(self):
        """Testa o acesso anônimo à view confirm_exclui_usuario"""
        url = reverse('usuarios:confirm_exclui_usuario', args=[1])
        response = self.client.get(path=url)
        url = settings.LOGIN_URL + '?next=' + url
        self.assertRedirects(response, url)

    def test_acesso_anonimo_adiciona_usuario_grupo(self):
        """Testa o acesso anônimo à view adiciona_usuario_grupo"""
        url = reverse('usuarios:adiciona_usuario_grupo', args=[1])
        response = self.client.get(path=url)
        url = settings.LOGIN_URL + '?next=' + url
        self.assertRedirects(response, url)

    def test_acesso_anonimo_troca_senha(self):
        """Testa o acesso anônimo à view troca_senha"""
        url = reverse('usuarios:troca_senha')
        response = self.client.get(path=url)
        url = settings.LOGIN_URL + '?next=' + url
        self.assertRedirects(response, url)

    def test_acesso_anonimo_esqueci_senha(self):
        """Testa o acesso anônimo à view esqueci_senha"""
        url = reverse('usuarios:esqueci_senha')
        response = self.client.get(path=url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'usuarios/gerenciamento/esqueci_senha.html')

    def test_acesso_anonimo_lista_grupos(self):
        """Testa o acesso anônimo à view lista_grupos"""
        url = reverse('usuarios:lista_grupos')
        response = self.client.get(path=url)
        url = settings.LOGIN_URL + '?next=' + url
        self.assertRedirects(response, url)

    def test_acesso_anonimo_criar_grupo(self):
        """Testa o acesso anônimo à view criar_grupo"""
        url = reverse('usuarios:criar_grupo')
        response = self.client.get(path=url)
        url = settings.LOGIN_URL + '?next=' + url
        self.assertRedirects(response, url)

    def test_acesso_anonimo_detalhe_grupo(self):
        """Testa o acesso anônimo à view detalhe_grupo"""
        url = reverse('usuarios:detalhe_grupo', args=[1])
        response = self.client.get(path=url)
        url = settings.LOGIN_URL + '?next=' + url
        self.assertRedirects(response, url)

    def test_acesso_anonimo_add_usuarios_grupo(self):
        """Testa o acesso anônimo à view add_usuarios_grupo"""
        url = reverse('usuarios:add_usuarios_grupo', args=[1])
        response = self.client.get(path=url)
        url = settings.LOGIN_URL + '?next=' + url
        self.assertRedirects(response, url)

    def test_acesso_anonimo_exclui_usuario_grupo(self):
        """Testa o acesso anônimo à view exclui_usuario_grupo"""
        url = reverse('usuarios:exclui_usuario_grupo', args=['foo', 1, 1])
        response = self.client.get(path=url)
        url = settings.LOGIN_URL + '?next=' + url
        self.assertRedirects(response, url)

    def test_acesso_anonimo_confirm_exclui_usuario_grupo(self):
        """Testa o acesso anônimo à view confirm_exclui_usuario_grupo"""
        url = reverse('usuarios:confirm_exclui_usuario_grupo', args=[1, 1])
        response = self.client.get(path=url)
        url = settings.LOGIN_URL + '?next=' + url
        self.assertRedirects(response, url)

    def test_acesso_anonimo_exclui_grupo(self):
        """Testa o acesso anônimo à view exclui_grupo"""
        url = reverse('usuarios:exclui_grupo', args=[1])
        response = self.client.get(path=url)
        url = settings.LOGIN_URL + '?next=' + url
        self.assertRedirects(response, url)

    def test_acesso_anonimo_confirm_exclui_grupo(self):
        """Testa o acesso anônimo à view confirm_exclui_grupo"""
        url = reverse('usuarios:confirm_exclui_grupo', args=[1])
        response = self.client.get(path=url)
        url = settings.LOGIN_URL + '?next=' + url
        self.assertRedirects(response, url)

    def test_acesso_anonimo_busca_permissoes(self):
        """Testa o acesso anônimo à view busca_permissoes """
        url = reverse('usuarios:busca_permissoes')
        response = self.client.get(path=url)
        url = settings.LOGIN_URL + '?next=' + url
        self.assertRedirects(response, url)

    def test_acesso_anonimo_add_permissoes(self):
        """Testa o acesso anônimo à view add_permissoes"""
        url = reverse('usuarios:add_permissoes', args=[1])
        response = self.client.get(path=url)
        url = settings.LOGIN_URL + '?next=' + url
        self.assertRedirects(response, url)

    def test_acesso_anonimo_exclui_permissao(self):
        """Testa o acesso anônimo à view exclui_permissao"""
        url = reverse('usuarios:exclui_permissao', args=[1, 1])
        response = self.client.get(path=url)
        url = settings.LOGIN_URL + '?next=' + url
        self.assertRedirects(response, url)

    def test_acesso_anonimo_confirm_exclui_permissao_grupo(self):
        """Testa o acesso anônimo à view confirm_exclui_permissao_grupo"""
        url = reverse('usuarios:confirm_exclui_permissao_grupo', args=[1, 1])
        response = self.client.get(path=url)
        url = settings.LOGIN_URL + '?next=' + url
        self.assertRedirects(response, url)


# #############################################################################
# ################ Testes com modificações na base de dados ###################
# #############################################################################


class ConfirmExcluiUsuarioViewTestCase(PermissionsTestCase):
    """ Case de testes para a view confirm_exclui_usuario """

    def test_acesso_sem_perm_confirm_exclui_usuario(self):
        """ Testa o acesso com usuário logado sem permissão da view confirm_exclui_usuario """
        self.create_user()
        self.login()
        temp = self.create_user("bar", "bar@teste")
        response = self.client.get(follow=True,
                                   path=reverse("usuarios:confirm_exclui_usuario",
                                                kwargs={'id_usuario': temp.id},
                                                ),
                                   )
        self.assertEqual(response.status_code, 403)

    def test_confirm_exclui_usuario(self):
        """ Testa o retorno correto da view confirm_exclui_usuario """
        user = self.create_user()
        user = self.set_perm(perm_name="delete_usuario", user_id=user.id)
        temp = self.create_user(nome_usuario='temp', email='temp@foo.com')
        self.login()
        # testa se a página é renderizada
        response = self.client.get(reverse("usuarios:confirm_exclui_usuario",
                                           kwargs={'id_usuario': temp.id}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'usuarios/gerenciamento/confirma_exclusao.html')

        # testa se o usuário é inativado
        response = self.client.post(path=reverse(viewname="usuarios:confirm_exclui_usuario",
                                                 kwargs={'id_usuario': temp.id},
                                                 ),
                                    data={'foo': 'foo'})
        temp = User.objects.get(pk=temp.id)
        self.assertFalse(temp.is_active)
        self.assertRedirects(response, reverse('usuarios:lista_usuarios'))


class ConfirmExcluiUsuarioGrupoViewTestCase(PermissionsTestCase):
    """ Case de testes para a view confirm_exclui_usuario_grupo """

    def test_acesso_sem_perm_confirm_exclui_usuario_grupo(self):
        """ Testa o acesso com usuário logado sem permissão da view confirm_exclui_usuario_grupo """
        user = self.create_user()
        self.login()
        grupo = self.create_group('foo')
        response = self.client.get(follow=True,
                                   path=reverse("usuarios:confirm_exclui_usuario_grupo",
                                                kwargs={'id_usuario': user.id,
                                                        'id_grupo': grupo.id},
                                                ),
                                   )
        self.assertEqual(response.status_code, 403)

    def test_confirm_exclui_usuario_grupo(self):
        """ Testa o acesso com usuário logado com permissão da view confirm_exclui_usuario_grupo """
        user = self.create_user()
        user = self.set_perm(perm_name="add_group", user_id=user.id)
        user = self.set_perm(perm_name="change_group", user_id=user.id)
        self.login()
        grupo = self.create_group('bar')
        temp = self.create_user(nome_usuario='temp', email='temp@foo.com')
        grupo.user_set.add(temp)

        # testa se a página é renderizada
        response = self.client.get(follow=True,
                                   path=reverse(viewname="usuarios:confirm_exclui_usuario_grupo",
                                                kwargs={'id_usuario': temp.id,
                                                        'id_grupo': grupo.id},
                                                ),
                                   )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('usuarios/gerenciamento/confirma_exclusao.html')

        # testa se o usuário é removido do grupo
        response = self.client.post(path=reverse(viewname="usuarios:confirm_exclui_usuario_grupo",
                                                 kwargs={'id_usuario': temp.id,
                                                         'id_grupo': grupo.id}),
                                    data={'foo': 'foo'})
        temp = User.objects.get(pk=temp.id)
        self.assertFalse(temp.groups.filter(name=grupo.name).exists())
        self.assertRedirects(response, reverse('usuarios:detalhe_grupo', args=[grupo.id]))


class ConfirmExcluiPermissaoGrupoViewTestCase(PermissionsTestCase):
    """ Case de testes para a view confirm_exclui_permissao_grupo """

    def test_acesso_sem_perm_confirm_exclui_permissao_grupo(self):
        """ Testa o acesso com usuário logado sem permissão da view confirm_exclui_permissao_grupo """
        user = self.create_user()
        self.login()
        grupo = self.create_group('foo')
        response = self.client.get(follow=True,
                                   path=reverse("usuarios:confirm_exclui_permissao_grupo",
                                                kwargs={'id_permissao': 1,
                                                        'id_grupo': grupo.id},
                                                ),
                                   )
        self.assertEqual(response.status_code, 403)

    def test_confirm_exclui_permissao_grupo(self):
        """ Testa o acesso com usuário logado com permissão da view confirm_exclui_permissao_grupo """
        perm = Permission.objects.get(codename="change_group")
        user = self.create_user()
        user = self.set_perm(perm_name="add_group", user_id=user.id)
        user = self.set_perm(perm_name=perm.codename, user_id=user.id)
        grupo = self.create_group('bar')
        grupo = self.set_perm(perm_name=perm.codename, group_id=grupo.id)
        self.login()

        # testa se a página é carregada
        response = self.client.get(path=reverse("usuarios:confirm_exclui_permissao_grupo",
                                                kwargs={'id_permissao': perm.id,
                                                        'id_grupo': grupo.id},
                                                ),
                                   )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('usuarios/gerenciamento/confirma_exclusao.html')

        # testa se a permissão é removida
        response = self.client.post(path=reverse("usuarios:confirm_exclui_permissao_grupo",
                                                 kwargs={'id_permissao': perm.id,
                                                         'id_grupo': grupo.id},
                                                 ),
                                    data={'foo': 'foo'})
        self.assertFalse(perm in grupo.permissions.all())
        self.assertRedirects(response, reverse('usuarios:detalhe_grupo', args=[grupo.id]))


class ConfirmExcluiGrupoViewTestCase(PermissionsTestCase):
    """ Case de testes para a view confirm_exclui_grupo """

    def test_acesso_sem_perm_confirm_exclui_grupo(self):
        """ Testa o acesso com usuário logado sem permissão da view confirm_exclui_grupo """
        user = self.create_user()
        grupo = self.create_group('bar')
        self.login()
        response = self.client.get(follow=True,
                                   path=reverse("usuarios:confirm_exclui_grupo",
                                                kwargs={'id_grupo': grupo.id},
                                                ),
                                   )
        self.assertEqual(response.status_code, 403)

    def test_confirm_exclui_grupo(self):
        """ Testa o acesso com usuário logado com permissão da view confirm_exclui_grupo """
        user = self.create_user()
        user = self.set_perm(perm_name='add_group', user_id=user.id)
        user = self.set_perm(perm_name='delete_group', user_id=user.id)
        grupo = self.create_group('bar')
        self.login()

        # testa se a página é carregada
        response = self.client.get(path=reverse("usuarios:confirm_exclui_grupo",
                                                kwargs={'id_grupo': grupo.id},
                                                ),
                                   )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('usuarios/gerenciamento/confirma_exclusao.html')

        # testa se o grupo é excluído
        response = self.client.post(path=reverse("usuarios:confirm_exclui_grupo",
                                                 kwargs={'id_grupo': grupo.id}),
                                    data={'foo': 'foo'})
        with self.assertRaises(Group.DoesNotExist):
            grupo = Group.objects.get(name='bar')
        self.assertRedirects(response, reverse('usuarios:lista_grupos'))


class ListaGruposViewTestCase(PermissionsTestCase):
    """ Case de testes para a view lista_grupos """

    def test_acesso_sem_perm_lista_grupos(self):
        """ Testa o acesso com usuário logado sem permissão da view lista_grupos """
        user = self.create_user()
        self.login()
        response = self.client.get(follow=True,
                                   path=reverse("usuarios:lista_grupos"))
        self.assertEqual(response.status_code, 403)

    def test_acesso_com_perm_lista_grupos(self):
        """ Testa o acesso com usuário logado com permissão da view lista_grupos """
        user = self.create_user()
        user = self.set_perm(perm_name='add_group', user_id=user.id)
        self.login()
        response = self.client.get(follow=True,
                                   path=reverse("usuarios:lista_grupos"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('usuarios/gerenciamento/grupos.html')

    def test_get_lista_grupos_pagina_invalida(self):
        '''Testa o retorno para um get com página inválida'''
        user = self.create_user()
        user = self.set_perm(perm_name='add_group', user_id=user.id)
        self.login()
        grupo = self.create_group('bar')
        url = reverse("usuarios:lista_grupos") + '?page=2'
        response = self.client.get(path=url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('usuarios/gerenciamento/grupos.html')
        self.assertContains(response, grupo.name)


class CriarGrupoViewTestCase(PermissionsTestCase):
    """ Case de testes para a view criar_grupo """

    def test_acesso_sem_perm_criar_grupo(self):
        """ Testa o acesso com usuário logado sem permissão da view criar_grupo """
        user = self.create_user()
        self.login()
        response = self.client.get(follow=True,
                                   path=reverse("usuarios:criar_grupo"))
        self.assertEqual(response.status_code, 403)

    def test_acesso_com_perm_criar_grupo_sem_post(self):
        """ Testa se o acesso com usuário logado com permissão da view \
        criar_grupo sem post gera exceção ValueError """
        user = self.create_user()
        user = self.set_perm(perm_name='add_group', user_id=user.id)
        self.login()
        with self.assertRaises(ValueError):
            self.client.get(path=reverse("usuarios:criar_grupo"))

    def test_acesso_com_perm_criar_grupo_com_post(self):
        """Testa o post da view criar_grupo"""
        user = self.create_user()
        user = self.set_perm(perm_name='add_group', user_id=user.id)
        self.login()
        data = {'nome': 'Foo Group'}
        response = self.client.post(path=reverse("usuarios:criar_grupo"), data=data)
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed('usuarios/gerenciamento/detalhe_grupo.html')
        self.assertTrue(Group.objects.filter(name='Foo Group'))


class DetalheGrupoViewTestCase(PermissionsTestCase):
    """ Case de testes para a view detalhe_grupo """

    def test_acesso_sem_perm_detalhe_grupo(self):
        """ Testa o acesso com usuário logado sem permissão da view detalhe_grupo """
        self.create_user()
        grupo = self.create_group('bar')
        self.login()
        response = self.client.get(follow=True,
                                   path=reverse("usuarios:detalhe_grupo",
                                                kwargs={'id_grupo': grupo.id},
                                                ),
                                   )
        self.assertEqual(response.status_code, 403)

    def test_acesso_com_perm_detalhe_grupo(self):
        """ Testa o acesso com usuário logado com permissão da view detalhe_grupo """
        user = self.create_user()
        user = self.set_perm(perm_name='add_group', user_id=user.id)
        grupo = self.create_group('bar')
        self.login()
        response = self.client.get(follow=True,
                                   path=reverse("usuarios:detalhe_grupo",
                                                kwargs={'id_grupo': grupo.id},
                                                ),
                                   )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('usuarios/gerenciamento/detalhe_grupo.html')

    def test_get_lista_usuarios_pagina_invalida(self):
        '''Testa o retorno para um get com página inválida'''
        user = self.create_user()
        user = self.set_perm(perm_name='add_group', user_id=user.id)
        grupo = self.create_group('bar')
        grupo.user_set.add(user)
        grupo.save()
        self.login()
        url = reverse("usuarios:detalhe_grupo", args=[grupo.id]) + '?page=2'
        response = self.client.get(path=url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('usuarios/gerenciamento/grupos.html')
        self.assertContains(response, user.nome_usuario)


class BuscaPermissoesViewTestCase(PermissionsTestCase):
    """ Case de testes para a view busca_permissoes """

    def test_acesso_sem_perm_busca_permissoes(self):
        """ Testa o acesso com usuário logado sem permissão da view busca_permissoes """
        self.create_user()
        self.login()
        response = self.client.get(follow=True,
                                   path=reverse("usuarios:busca_permissoes"))
        self.assertEqual(response.status_code, 403)

    def test_acesso_com_perm_busca_permissoes(self):
        """ Testa o acesso com usuário logado com permissão da view busca_permissoes """
        user = self.create_user()
        user = self.set_perm(perm_name='add_permission', user_id=user.id)
        self.login()
        url = reverse("usuarios:busca_permissoes") + '?q=can'
        response = self.client.get(follow=True,
                                   path=url)
        self.assertEqual(response.status_code, 200)


class BuscaUsuariosJSONViewTestCase(PermissionsTestCase):
    """ Case de testes para a view busca_usuarios_json """

    def test_acesso_sem_perm_busca_usuarios_json(self):
        """ Testa o acesso com usuário logado sem permissão da view busca_usuarios_json """
        self.create_user()
        self.login()
        response = self.client.get(follow=True,
                                   path=reverse("usuarios:busca_usuarios_json"))
        self.assertEqual(response.status_code, 403)

    def test_acesso_com_perm_busca_usuarios_json(self):
        """ Testa o acesso com usuário logado com permissão da view busca_usuarios_json """
        user = self.create_user(nome_completo='Foo Tester')
        user = self.set_perm(perm_name='add_usuario', user_id=user.id)
        self.login()
        url = reverse("usuarios:busca_usuarios_json") + '?q=foo'
        response = self.client.get(follow=True,
                                   path=url)
        self.assertEqual(response.status_code, 200)


class AddUsuariosGrupoViewTestCase(PermissionsTestCase):
    """ Case de testes para a view add_usuarios_grupo """

    def test_acesso_sem_perm_add_usuarios_grupo(self):
        """ Testa o acesso com usuário logado sem permissão da view add_usuarios_grupo """
        self.create_user()
        self.login()
        response = self.client.get(follow=True,
                                   path=reverse("usuarios:add_usuarios_grupo",
                                                kwargs={'id_grupo': 1},
                                                ),
                                   )
        self.assertEqual(response.status_code, 403)

    def test_add_usuarios_grupo(self):
        """ Testa o acesso com usuário logado com permissão da view add_usuarios_grupo """
        user = self.create_user()
        user = self.set_perm(perm_name='add_group', user_id=user.id)
        user = self.set_perm(perm_name='change_group', user_id=user.id)
        self.login()
        grupo = self.create_group('bar')
        response = self.client.post(path=reverse("usuarios:add_usuarios_grupo",
                                                 kwargs={'id_grupo': grupo.id}),
                                    data={'nome': str(user.id) + ',-1'})
        self.assertTrue(grupo.user_set.filter(pk=user.id).exists())
        self.assertRedirects(response, reverse('usuarios:detalhe_grupo', args=[grupo.id]))


class ExcluiUsuarioGrupoViewTestCase(PermissionsTestCase):
    """ Case de testes para a view exclui_usuario_grupo """

    def test_acesso_sem_perm_exclui_usuario_grupo(self):
        """ Testa o acesso com usuário logado sem permissão da view exclui_usuario_grupo """
        self.create_user()
        self.login()
        response = self.client.get(follow=True,
                                   path=reverse("usuarios:exclui_usuario_grupo",
                                                kwargs={'origem': None,
                                                        'id_grupo': 1,
                                                        'id_usuario': 1},
                                                ),
                                   )
        self.assertEqual(response.status_code, 403)

    def test_acesso_com_perm_exclui_usuario_grupo(self):
        """ Testa o acesso com usuário logado com permissão da view exclui_usuario_grupo """
        user = self.create_user()
        user = self.set_perm(perm_name='change_group', user_id=user.id)
        user = self.set_perm(perm_name='add_group', user_id=user.id)
        grupo = self.create_group('bar')
        self.login()
        response = self.client.get(follow=True,
                                   path=reverse("usuarios:exclui_usuario_grupo",
                                                kwargs={'origem': 'detalhe_usuario',
                                                        'id_grupo': grupo.id,
                                                        'id_usuario': user.id},
                                                ),
                                   )
        self.assertEqual(response.status_code, 200)


class AddPermissoesViewTestCase(PermissionsTestCase):
    """ Case de testes para a view add_permissoes """

    def test_acesso_sem_perm_add_permissoes(self):
        """ Testa o acesso com usuário logado sem permissão da view add_permissoes """
        self.create_user()
        self.login()
        response = self.client.get(follow=True,
                                   path=reverse("usuarios:add_permissoes",
                                                kwargs={'id_grupo': 1},
                                                ),
                                   )
        self.assertEqual(response.status_code, 403)

    def test_add_permissoes(self):
        """ Testa o retorno da view add_permissoes """
        user = self.create_user()
        user = self.set_perm(perm_name='change_group', user_id=user.id)
        user = self.set_perm(perm_name='add_group', user_id=user.id)
        grupo = self.create_group('bar')
        perm = Permission.objects.first()
        self.login()
        response = self.client.post(follow=True,
                                    path=reverse("usuarios:add_permissoes",
                                                 kwargs={'id_grupo': grupo.id},
                                                 ),
                                    data={'nome': perm.id}
                                    )
        self.assertTrue(grupo.permissions.filter(pk=perm.id).exists())
        self.assertRedirects(response, reverse('usuarios:detalhe_grupo', args=[grupo.id]))

    def test_add_permissoes_except(self):
        '''Testa retorno da view add_permissoes para permissões não existentes'''
        user = self.create_user()
        user = self.set_perm(perm_name='change_group', user_id=user.id)
        user = self.set_perm(perm_name='add_group', user_id=user.id)
        grupo = self.create_group('bar')
        self.login()
        response = self.client.post(follow=True,
                                    path=reverse("usuarios:add_permissoes",
                                                 kwargs={'id_grupo': grupo.id},
                                                 ),
                                    data={'nome': -1}
                                    )
        self.assertRedirects(response, reverse('usuarios:detalhe_grupo', args=[grupo.id]))


class ExcluiPermissaoViewTestCase(PermissionsTestCase):
    """ Case de testes para a view exclui_permissao """

    def test_acesso_sem_perm_exclui_permissao(self):
        """ Testa o acesso com usuário logado sem permissão da view exclui_permissao """
        self.create_user()
        self.login()
        response = self.client.get(follow=True,
                                   path=reverse("usuarios:exclui_permissao",
                                                kwargs={'id_grupo': 1,
                                                        'id_permissao': 1},
                                                ),
                                   )
        self.assertEqual(response.status_code, 403)

    def test_acesso_com_perm_exclui_permissao(self):
        """ Testa o acesso com usuário logado com permissão da view exclui_permissao """
        user = self.create_user()
        perm = Permission.objects.get(codename='change_group')
        user = self.set_perm(perm_name=perm.codename, user_id=user.id)
        user = self.set_perm(perm_name='add_group', user_id=user.id)
        grupo = self.create_group('bar')
        self.login()
        response = self.client.get(follow=True,
                                   path=reverse("usuarios:exclui_permissao",
                                                kwargs={'id_grupo': grupo.id,
                                                        'id_permissao': perm.id},
                                                ),
                                   )
        self.assertEqual(response.status_code, 200)


class ExcluiGrupoViewTestCase(PermissionsTestCase):
    """ Case de testes para a view exclui_grupo """

    def test_acesso_sem_perm_exclui_grupo(self):
        """ Testa o acesso com usuário logado sem permissão da view exclui_grupo """
        self.create_user()
        self.login()
        response = self.client.get(follow=True,
                                   path=reverse("usuarios:exclui_grupo",
                                                kwargs={'id_grupo': 1},
                                                ),
                                   )
        self.assertEqual(response.status_code, 403)

    def test_acesso_com_perm_exclui_grupo(self):
        """ Testa o acesso com usuário logado com permissão da view exclui_grupo """
        user = self.create_user()
        user = self.set_perm(perm_name='delete_group', user_id=user.id)
        user = self.set_perm(perm_name='add_group', user_id=user.id)
        grupo = self.create_group('bar')
        grupo.user_set.add(user)
        grupo.save()
        self.login()
        response = self.client.get(path=reverse("usuarios:exclui_grupo",
                                                kwargs={'id_grupo': grupo.id}))
        with self.assertRaises(Group.DoesNotExist):
            Group.objects.get(pk=grupo.id)

        for u in User.objects.all():
            with self.assertRaises(Group.DoesNotExist):
                u.groups.get(pk=grupo.id)

        self.assertRedirects(response, reverse('usuarios:lista_grupos'))


class ListaUsuariosViewTestCase(PermissionsTestCase):
    """ Case de testes para a view lista_usuarios """

    def test_acesso_lista_usuarios(self):
        """ Testa o acesso com usuário logado da view lista_usuarios """
        self.create_user()
        self.login()
        response = self.client.get(path=reverse("usuarios:lista_usuarios"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'usuarios/gerenciamento/usuarios.html')

    def test_acesso_lista_usuarios_pagina_invalida(self):
        """ Testa o acesso com usuário logado de uma página inválida da view lista_usuarios """
        self.create_user()
        self.login()
        url = reverse("usuarios:lista_usuarios") + '?page=2'
        response = self.client.get(path=url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'usuarios/gerenciamento/usuarios.html')

    def test_busca_usuario_lista_usuarios(self):
        '''Testa o formulário de busca de usuários'''
        self.create_user()
        self.create_user(nome_usuario='outronome', email='foo@outroemail.com')
        self.login()
        response = self.client.post(path=reverse("usuarios:lista_usuarios"),
                                    data={'busca_email': 'foo'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'usuarios/gerenciamento/usuarios.html')


class AdicionaUsuarioGrupoViewTestCase(PermissionsTestCase):
    """ Case de testes para a view adiciona_usuario_grupo """

    def test_acesso_sem_perm_adiciona_usuario_grupo(self):
        """ Testa o acesso com usuário logado sem permissão da view adiciona_usuario_grupo """
        self.create_user()
        self.login()
        response = self.client.get(follow=True,
                                   path=reverse("usuarios:adiciona_usuario_grupo",
                                                kwargs={'id_usuario': 1},
                                                ),
                                   )
        self.assertEqual(response.status_code, 403)

    def test_adiciona_usuario_grupo(self):
        """ Testa o acesso com usuário logado com permissão da view adiciona_usuario_grupo """
        user = self.create_user()
        user = self.set_perm(perm_name='change_group', user_id=user.id)
        self.login()

        # acesso sem post
        response = self.client.get(path=reverse("usuarios:adiciona_usuario_grupo",
                                                kwargs={'id_usuario': user.id}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'usuarios/gerenciamento/detalhe_usuario.html')

        # acesso com post
        grupo = self.create_group('bar')
        response = self.client.post(path=reverse('usuarios:adiciona_usuario_grupo',
                                                 kwargs={'id_usuario': user.id}),
                                    data={'grupo': grupo.id})
        self.assertTrue(grupo.user_set.filter(pk=user.id).exists())
        self.assertRedirects(response, reverse("usuarios:detalhe_usuario", args=[user.id]))


class RestauraUsuarioSistemaViewTestCase(PermissionsTestCase):
    """ Case de testes para a view restaura_usuario_sistema """

    def test_acesso_sem_perm_restaura_usuario_sistema(self):
        """ Testa o acesso com usuário logado sem permissão da view restaura_usuario_sistema """
        self.create_user()
        self.login()
        response = self.client.get(follow=True,
                                   path=reverse("usuarios:restaura_usuario",
                                                kwargs={'id_usuario': 1},
                                                ),
                                   )
        self.assertEqual(response.status_code, 403)

    def test_acesso_com_perm_restaura_usuario_sistema(self):
        """ Testa o acesso com usuário logado com permissão da view restaura_usuario_sistema """
        user = self.create_user()
        user = self.set_perm(perm_name='change_usuario', user_id=user.id)
        self.login()
        response = self.client.get(follow=True,
                                   path=reverse("usuarios:restaura_usuario",
                                                kwargs={'id_usuario': user.id},
                                                ),
                                   )
        self.assertEqual(response.status_code, 200)


class EditarUsuarioSistemaViewTestCase(PermissionsTestCase):
    """ Case de testes para a view editar_usuario_sistema """

    def test_acesso_sem_perm_criar_usuario_sistema(self):
        """ Testa o acesso com usuário logado sem permissão da url criar_usuario_sistema """
        self.create_user()
        self.login()
        response = self.client.get(follow=True,
                                   path=reverse("usuarios:criar_usuario_sistema"))
        self.assertEqual(response.status_code, 403)

    def test_acesso_sem_perm_editar_usuario_sistema(self):
        """ Testa o acesso com usuário logado sem permissão da url editar_usuario_sistema """
        self.create_user()
        self.login()
        temp = self.create_user(nome_usuario='temp', email='temp@foo.com')
        response = self.client.get(follow=True,
                                   path=reverse("usuarios:editar_usuario_sistema", args=[temp.id]))
        self.assertEqual(response.status_code, 403)

    def test_criar_usuario_sistema(self):
        """ Testa o acesso com usuário logado com permissão da url criar_usuario_sistema """
        user = self.create_user()
        user = self.set_perm(perm_name='add_usuario', user_id=user.id)
        self.login()

        # acesso via get
        response = self.client.get(path=reverse("usuarios:criar_usuario_sistema"))
        self.assertEqual(response.status_code, 200)

        # acesso via post
        model = get_user_model()
        if model._meta.app_label == "auth" and model._meta.model_name == "user":
            response = self.client.post(path=reverse("usuarios:criar_usuario_sistema"),
                                        data={'username': 'teste',
                                              'first_name': 'Teste',
                                              'last_name': 'Foo',
                                              'email': 'teste@foo.com'})
            temp = get_object_or_404(User, username='teste')
        else:
            response = self.client.post(path=reverse("usuarios:criar_usuario_sistema"),
                                        data={'nome_usuario': 'teste',
                                              'nome_completo': 'Teste Foo',
                                              'email': 'teste@foo.com'})
            temp = get_object_or_404(User, nome_usuario='teste')
        self.assertRedirects(response, reverse('usuarios:detalhe_usuario', args=[temp.id]))

    def test_editar_usuario_sistema(self):
        """ Testa o acesso com usuário logado com permissão da url editar_usuario_sistema """
        user = self.create_user()
        user = self.set_perm(perm_name='change_usuario', user_id=user.id)
        temp = self.create_user(nome_usuario='temp', email='temp@foo.com')
        self.login()

        # acesso via get
        response = self.client.get(path=reverse("usuarios:editar_usuario_sistema", args=[temp.id]))
        self.assertEqual(response.status_code, 200)

        # acesso via post
        model = get_user_model()
        response = self.client.post(reverse("usuarios:editar_usuario_sistema", args=[temp.id]),
                                    {'nome_completo': 'Modificado Foo',
                                     'email': 'modificado@foo.com'})

        if model._meta.app_label == "auth" and model._meta.model_name == "user":
            temp = get_object_or_404(User, username='temp')
        else:
            temp = get_object_or_404(User, nome_usuario='temp')

        self.assertEqual(temp.get_full_name(), 'Modificado Foo')
        self.assertRedirects(response, reverse('usuarios:detalhe_usuario', args=[temp.id]))

    def test_form_grupos(self):
        '''Testa o formulário de adicionar usuário a grupo'''
        user = self.create_user()
        user = self.set_perm(perm_name='change_group', user_id=user.id)
        grupo = self.create_group('bar')
        self.login()

        # acesso via get
        response = self.client.get(reverse("usuarios:editar_usuario_sistema", args=[user.id]))
        self.assertContains(response, 'Adicionar novo grupo')

        # acesso via post
        response = self.client.post(reverse("usuarios:editar_usuario_sistema", args=[user.id]),
                                    data={'grupo': grupo.id})
        self.assertTrue(grupo.user_set.filter(pk=user.id).exists())
        self.assertRedirects(response, reverse("usuarios:detalhe_usuario", args=[user.id]))


class LoginViewTestCase(PermissionsTestCase):
    '''Case de testes para view de login'''

    def test_login_success(self):
        '''Testa se o usuário consegue realizar login'''
        user = self.create_user()
        response = self.client.post(path=reverse('usuarios:login'),
                                    data={'nome_usuario': user.nome_usuario,
                                          'senha': 'footeste'})
        self.assertEqual(response.status_code, 302)

    def test_login_error(self):
        '''Testa se o login gera erros'''
        user = self.create_user()

        # login com senha errada
        response = self.client.post(path=reverse('usuarios:login'),
                                    data={'nome_usuario': user.nome_usuario,
                                          'senha': 'senhaerrada'})
        msg = u'Usuário/senha incorreto'
        self.assertContains(response, msg, status_code=200, html=False)

        # login com nome de usuário errado
        response = self.client.post(path=reverse('usuarios:login'),
                                    data={'nome_usuario': 'uber',
                                          'senha': 'senhaerrada'})
        self.assertContains(response, msg, status_code=200, html=False)

        # login com usuário inativo
        response = self.client.post(path=reverse('usuarios:login'),
                                    data={'nome_usuario': user.nome_usuario,
                                          'senha': 'senhaerrada'})
        user.is_active = False
        user.save()
        self.assertContains(response, msg, status_code=200, html=False)


class LogoutViewTestCase(PermissionsTestCase):
    '''Case de testes para view de logout'''

    def test_logout(self):
        '''Testa se o usuário é deslogado'''
        user = self.create_user()
        response = self.client.post(path=reverse('usuarios:login'),
                                    data={'nome_usuario': user.nome_usuario,
                                          'senha': 'footeste'})
        response = self.client.get(reverse('usuarios:logout'))
        self.assertEqual(response.status_code, 302)
        with self.assertRaises(AttributeError):
            print response.user


class EsqueciSenhaViewTestCase(PermissionsTestCase):
    '''Case de testes para view esqueci_senha'''

    def test_post_esqueci_senha(self):
        '''Testa o retorno do POST válido'''
        user = self.create_user()
        self.login()

        # email válido
        response = self.client.post(path=reverse('usuarios:esqueci_senha'),
                                    data={'email': user.email})
        self.assertRedirects(response, '/')

        # email inválido
        response = self.client.post(path=reverse('usuarios:esqueci_senha'),
                                    data={'email': 'emailinvalido@foo.com'})
        self.assertRedirects(response, '/')


class ResetaSenhaViewTestCase(PermissionsTestCase):
    """ Case de testes da view reseta_senha """

    def test_get_reseta_senha(self):
        '''Testa o get da view reseta_senha'''
        user = self.create_user()
        self.login()
        kwargs_dict = {'uidb64': urlsafe_base64_encode(force_bytes(user.pk)),
                       'token': default_token_generator.make_token(user)}
        response = self.client.get(path=reverse('usuarios:reseta_senha',
                                                kwargs=kwargs_dict))
        self.assertEqual(response.status_code, 200)


class TrocaSenhaViewTestCase(PermissionsTestCase):
    '''Case de testes da view troca_senha'''

    def test_get_troca_senha(self):
        '''Testa o retorno da view'''
        self.create_user()
        self.login()
        response = self.client.get(path=reverse('usuarios:troca_senha'))
        self.assertEqual(response.status_code, 200)
