# coding: utf-8
from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.contrib.auth.models import Group, Permission
from django.utils.translation import ugettext as _
from comum.fields import TrimmedCharFormField
from django.conf import settings


CUSTOM_FIELDS = ['nome_usuario',
                 'nome_completo',
                 'email',
                 'is_staff',
                 'is_superuser',
                 'is_active']

AUTH_FIELDS = ['username',
               'first_name',
               'last_name',
               'email',
               'is_staff',
               'is_superuser',
               'is_active']


def setup_fields():
    model = get_user_model()
    if model._meta.app_label == "auth" and model._meta.model_name == "user":
        return AUTH_FIELDS
    else:
        return CUSTOM_FIELDS


class LoginForm(forms.Form):

    nome_usuario = forms.CharField(label=_(u"Nome de Usuário"),
                                   help_text=_(u"Nome do usuário registrado."),
                                   max_length=30)

    senha = forms.CharField(label=_(u"Senha"),
                            help_text=_(u"Senha do usuário"),
                            max_length=64,
                            widget=forms.PasswordInput())


class GroupPermissionForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(GroupPermissionForm, self).__init__(*args, **kwargs)
        self.fields['permissoes'].initial = Permission.objects.filter(
            pk__in=self.instance.permissions.all()
        )
    if hasattr(settings, 'LISTA_PERMISSOES'):
        permissoes = forms.ModelMultipleChoiceField(
            queryset=Permission.objects.filter(
                codename__in=settings.LISTA_PERMISSOES
            ))
    else:
        permissoes = forms.ModelMultipleChoiceField(
                       queryset=Permission.objects.all())

    class Meta:
        model = Group
        fields = []


class UsuarioChangeForm(UserChangeForm):

    class Meta:
        model = get_user_model()
        fields = setup_fields()


class UsuarioCreateForm(UserCreationForm):

    class Meta:
        model = get_user_model()
        fields = setup_fields()


class AdminUsuarioCreate(forms.ModelForm):

    class Meta:
        model = get_user_model()
        fields = setup_fields()


class AdminUsuarioUpdate(forms.ModelForm):

    class Meta:
        model = get_user_model()
        if model._meta.app_label == "auth" and model._meta.model_name == "user":
            fields = ['username', 'email']
        else:
            fields = ['nome_completo', 'email']


class FormBuscaUsuario(forms.Form):

    """
    busca da pagina principal com varios campos
    atualizar a parte de extração de filtros
    """
    busca_nome_completo = TrimmedCharFormField(label=_(u'Nome completo'),
                                               max_length=50,
                                               required=False,)
    busca_email = TrimmedCharFormField(label=_(u'Email'),
                                       max_length=50,
                                       required=False,)

    search_params = {'nome_completo__icontains': 'busca_nome_completo',
                     'email__icontains': 'busca_email', }

    def extrair_filtro(self):
        if self.is_valid():
            params = {}
            for key in self.search_params:
                if self.cleaned_data[self.search_params[key]]:
                    params[key] = self.cleaned_data[self.search_params[key]]
            return params
        return None


class FormBuscaGrupo(forms.Form):
    """
    Busca da página principal com vários campos
    atualizar a parte de extração de filtros
    """
    busca_nome = TrimmedCharFormField(label=_(u'Nome do grupo'),
                                      max_length=50,
                                      required=False,)

    search_params = {
        'name__icontains': 'busca_nome',
    }

    def extrair_filtro(self):
        if self.is_valid():
            params = {}
            for key in self.search_params:
                if self.cleaned_data[self.search_params[key]]:
                    params[key] = self.cleaned_data[self.search_params[key]]
            return params
        return None


class FormUsuarioGrupo(forms.Form):
    grupo = forms.ModelChoiceField(queryset=Group.objects.all())


class EsqueciSenha(forms.Form):
    email = TrimmedCharFormField(label=_(u'Email'),
                                 required=False,
                                 max_length=50,)
