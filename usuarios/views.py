# coding: utf-8
import json
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model, authenticate, login as login_django, logout as logout_django
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group, Permission
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.views import password_reset_confirm, password_change
from django.contrib.sites.shortcuts import get_current_site
from django.core import serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.template.loader import get_template
from django.http import HttpResponse, HttpResponseForbidden
from django.shortcuts import render, redirect, get_object_or_404
from django.template import Context
from django.utils.encoding import force_bytes
from django.utils.translation import ugettext as _
from django.utils.http import urlsafe_base64_encode
from django.utils.timezone import now
from .forms import LoginForm, FormBuscaUsuario, FormBuscaGrupo, EsqueciSenha, GroupPermissionForm, AdminUsuarioCreate, AdminUsuarioUpdate, FormUsuarioGrupo
from .signals import (signal_excluir_usuarios_usuario,
                      signal_excluir_usuarios_grupo,
                      signal_editar_usuarios,
                      signal_restaurar_usuarios,
                      signal_usuario_add_grupo_usuarios,
                      signal_excluir_permissao_usuarios,
                      signal_usuario_excluir_grupo_usuarios,
                      signal_add_permissao_usuarios)


def check_perm(usuario,
               permissoes):

    if not usuario:
        return False

    if not permissoes:
        return False

    if isinstance(permissoes, basestring):
        permissoes = [permissoes]

    if usuario.user_permissions.filter(codename__in=permissoes).exists():
        return True

    for g in usuario.groups.all():
        if g.permissions.filter(codename__in=permissoes).exists():
            return True


def login(request):
    """View com código para login"""
    login_form = LoginForm(request.POST or None)

    if login_form.is_valid():

        username = login_form.cleaned_data["nome_usuario"]
        password = login_form.cleaned_data["senha"]

        usuario = authenticate(username=username,
                               password=password)

        if usuario is not None and usuario.is_active:

            usuario.ultimo_login = now()
            usuario.save()
            login_django(request, usuario)

            return redirect(settings.LOGIN_REDIRECT_URL)
        else:
            return render(request, "usuarios/login.html", {"login_form": login_form,
                                                           'error_message': _(u'Usuário/senha incorreto')})

    return render(request, "usuarios/login.html", {"login_form": login_form})


@login_required
def logout(request):
    logout_django(request)
    return redirect("usuarios:login")


@login_required
def confirm_exclui_usuario(request, id_usuario):
    """confirmação para a exclusão de um objeto"""
    if not check_perm(request.user, permissoes='delete_usuario'):
        return HttpResponseForbidden()
    usuario = get_object_or_404(get_user_model(), pk=id_usuario)
    if request.POST:
        exclui_usuario_sistema(request, id_usuario)
        return redirect('usuarios:lista_usuarios')
    else:
        ctx = {"objeto": usuario, "prev_link": reverse("usuarios:lista_usuarios")}
        return render(request, 'usuarios/gerenciamento/confirma_exclusao.html', ctx)


@login_required
def confirm_exclui_usuario_grupo(request, id_grupo, id_usuario):
    """confirmação para a exclusão de um objeto"""
    if not check_perm(request.user, permissoes='change_group'):
        return HttpResponseForbidden()
    usuario = get_object_or_404(get_user_model(), pk=id_usuario)
    if request.POST:
        exclui_usuario_grupo(request, 'grupo', id_grupo, id_usuario)
        return redirect('usuarios:detalhe_grupo', id_grupo=id_grupo)
    else:
        ctx = {"objeto": usuario, "prev_link": reverse("usuarios:detalhe_grupo", args=[id_grupo])}
        return render(request, 'usuarios/gerenciamento/confirma_exclusao.html', ctx)


@login_required
def confirm_exclui_permissao_grupo(request, id_grupo, id_permissao):
    """confirmação para a exclusão de um objeto"""
    if not check_perm(request.user, permissoes='change_group'):
        return HttpResponseForbidden()
    permissao = get_object_or_404(Permission, pk=id_permissao)
    if request.POST:
        exclui_permissao(request, id_grupo, id_permissao)
        return redirect('usuarios:detalhe_grupo', id_grupo=id_grupo)
    else:
        ctx = {"objeto": permissao, "prev_link": reverse("usuarios:detalhe_grupo", args=[id_grupo])}
        return render(request, 'usuarios/gerenciamento/confirma_exclusao.html', ctx)


@login_required
def confirm_exclui_grupo(request, id_grupo):
    """confirmação para a exclusão de um objeto"""
    if not check_perm(request.user, permissoes='delete_group'):
        return HttpResponseForbidden()
    grupo = get_object_or_404(Group, pk=id_grupo)
    if request.POST:
        exclui_grupo(request, id_grupo)
        return redirect('usuarios:lista_grupos')
    else:
        ctx = {"objeto": grupo, "prev_link": reverse("usuarios:lista_grupos")}
        return render(request, 'usuarios/gerenciamento/confirma_exclusao.html', ctx)


@login_required
def lista_grupos(request):
    '''exibe lista de grupos'''
    if not check_perm(request.user, permissoes='add_group'):
        return HttpResponseForbidden()
    form_busca = FormBuscaGrupo(request.POST or None)
    g = busca_grupos(request, form_busca)

    paginator = Paginator(g, 15)

    page = request.GET.get('page')

    try:
        grupos = paginator.page(page)
    except PageNotAnInteger:
        grupos = paginator.page(1)
    except EmptyPage:
        grupos = paginator.page(paginator.num_pages)

    ctx = {"grupos": grupos, "form_busca": form_busca}

    return render(request, 'usuarios/gerenciamento/grupos.html', ctx)


@login_required
def busca_grupos(request, form_busca):
    return _filtra_model(form_busca.extrair_filtro(), Group)


@login_required
def criar_grupo(request):
    # TODO: criar form e arrumar isto aqui
    if not check_perm(request.user, permissoes='add_group'):
        return HttpResponseForbidden()
    if request.POST:
        nome = request.POST['nome']
        grupo = Group.objects.create(name=nome)

        return redirect('usuarios:detalhe_grupo', id_grupo=grupo.id)


@login_required
def detalhe_grupo(request, id_grupo):
    if not check_perm(request.user, permissoes='add_group'):
        return HttpResponseForbidden()
    grupo = get_object_or_404(Group, pk=id_grupo)

    u = get_user_model().objects.filter(groups__pk=grupo.id)
    perm = grupo.permissions.all()

    pag_usua = Paginator(u, 20)

    page = request.GET.get('page')

    perm_form = GroupPermissionForm(
        request.POST or None, instance=grupo
    )

    if perm_form.is_valid():
        grupo.permissions.clear()
        for p in perm_form.cleaned_data['permissoes']:
            grupo.permissions.add(p)

        messages.success(
            request,
            u"Permissões do Grupo atualizadas com sucesso."
        )

    try:
        usu = pag_usua.page(page)
    except PageNotAnInteger:
        usu = pag_usua.page(1)
    except EmptyPage:
        usu = pag_usua.page(pag_usua.num_pages)

    ctx = {"grupo": grupo, "permissoes": perm, "usuarios": usu, "perm_form": perm_form}

    return render(request, 'usuarios/gerenciamento/detalhe_grupo.html', ctx)


@login_required
def busca_permissoes(request):
    """
    retorna lista json com os resultados para o tokeninput
    """
    if not check_perm(request.user, permissoes='add_permission'):
        return HttpResponseForbidden()
    p = request.GET['q']
    permissoes = Permission.objects.filter(name__icontains=p)
    js = serializers.serialize('json', permissoes, fields=('name'))
    j = json.loads(js)
    l = [i['fields'] for i in j]
    for i, v in enumerate(l):
        v.update({'id': j[i]['pk']})

    return HttpResponse(json.dumps(l), content_type='application/json')


@login_required
def busca_usuarios_json(request):
    """
    retorna lista json com os resultados para o tokeninput
    """
    if not check_perm(request.user, permissoes='add_usuario'):
        return HttpResponseForbidden()
    p = request.GET['q']
    usuarios = get_user_model().objects.filter(nome_completo__icontains=p)
    js = serializers.serialize('json', usuarios, fields=('nome_completo'))

    j = json.loads(js)

    l = [i['fields'] for i in j]
    for i, v in enumerate(l):
        v.update({'id': j[i]['pk']})

    for i in l:
        i['name'] = i.pop('nome_completo')

    return HttpResponse(json.dumps(l), content_type='application/json')


@login_required
def add_usuarios_grupo(request, id_grupo):
    if not check_perm(request.user, permissoes='change_group'):
        return HttpResponseForbidden()
    if request.POST:
        grupo = get_object_or_404(Group, pk=id_grupo)
        usuarios = request.POST['nome'].split(',')
        for i in usuarios:
            try:
                u = get_user_model().objects.get(pk=int(i))
                grupo.user_set.add(u)
                signal_usuario_add_grupo_usuarios.send(sender=grupo, usuario=u, grupo=grupo, autor=request.user)
            except:
                pass
        grupo.save()
        return redirect('usuarios:detalhe_grupo', id_grupo=grupo.id)


@login_required
def exclui_usuario_grupo(request, origem, id_grupo, id_usuario):
    if not check_perm(request.user, permissoes='change_group'):
        return HttpResponseForbidden()
    usuario = get_object_or_404(get_user_model(), pk=id_usuario)
    grupo = get_object_or_404(Group, pk=id_grupo)
    grupo.user_set.remove(usuario)
    grupo.save()
    signal_usuario_excluir_grupo_usuarios.send(sender=grupo, usuario=usuario, grupo=grupo, autor=request.user)
    if origem == "grupo":
        return redirect('usuarios:detalhe_grupo', id_grupo=id_grupo)
    else:
        return redirect('usuarios:detalhe_usuario', id_usuario=id_usuario)


@login_required
def add_permissoes(request, id_grupo):
    if not check_perm(request.user, permissoes='change_group'):
        return HttpResponseForbidden()
    if request.POST:
        grupo = get_object_or_404(Group, pk=id_grupo)
        permissoes = request.POST['nome'].split(',')

        for i in permissoes:
            try:
                p = Permission.objects.get(pk=int(i))
                grupo.permissions.add(p)
                signal_add_permissao_usuarios.send(sender=grupo, permissao=p, grupo=grupo, autor=request.user)
            except:
                pass
        grupo.save()

        return redirect('usuarios:detalhe_grupo', id_grupo=grupo.id)


@login_required
def exclui_permissao(request, id_grupo, id_permissao):
    if not check_perm(request.user, permissoes='change_group'):
        return HttpResponseForbidden()
    permissao = get_object_or_404(Permission, pk=id_permissao)
    grupo = get_object_or_404(Group, pk=id_grupo)
    grupo.permissions.remove(permissao)
    grupo.save()
    signal_excluir_permissao_usuarios.send(sender=grupo, permissao=permissao, grupo=grupo, autor=request.user)
    return redirect('usuarios:detalhe_grupo', id_grupo=grupo.id)


@login_required
def exclui_grupo(request, id_grupo):
    """ deve retirar todos os usuarios deste grupo
        antes de excluir
        Remover o usuário do sistema??
    """
    if not check_perm(request.user, permissoes='delete_group'):
        return HttpResponseForbidden()
    grupo = get_object_or_404(Group, pk=id_grupo)

    usuarios = get_user_model().objects.filter(groups__pk=grupo.id)

    for u in usuarios:
        u.groups.remove(grupo)
        u.save()

    grupo.delete()
    signal_excluir_usuarios_grupo.send(sender=None, grupo=grupo, autor=request.user)

    messages.success(request, u"O grupo '{}' excluído com sucesso.".format(grupo.name))
    return redirect('usuarios:lista_grupos')


@login_required
def lista_usuarios(request):
    form_busca = FormBuscaUsuario(request.POST or None)
    usuarios = busca_usuarios(request, form_busca)
    u_ativos = usuarios.filter(is_active=True)
    u_inativos = usuarios.filter(is_active=False)

    ativos = Paginator(u_ativos, 15)
    inativos = Paginator(u_inativos, 15)

    page = request.GET.get('page')

    try:
        usuarios_a = ativos.page(page)
    except PageNotAnInteger:
        usuarios_a = ativos.page(1)
    except EmptyPage:
        usuarios_a = ativos.page(ativos.num_pages)

    try:
        usuarios_i = inativos.page(page)
    except PageNotAnInteger:
        usuarios_i = inativos.page(1)
    except EmptyPage:
        usuarios_i = inativos.page(inativos.num_pages)

    ctx = {"usuarios_a": usuarios_a,
           "usuarios_i": usuarios_i,
           "form_busca": form_busca}

    return render(request, 'usuarios/gerenciamento/usuarios.html', ctx)


@login_required
def adiciona_usuario_grupo(request, id_usuario):
    """
    Adiciona um usuário a um grupo (usado na tela de edição de usuario)
    """
    if not check_perm(request.user, permissoes='change_group'):
        return HttpResponseForbidden()
    usuario = get_object_or_404(get_user_model(), pk=id_usuario)
    form = FormUsuarioGrupo(request.POST or None)

    if form.is_valid():
        grupo = form.cleaned_data['grupo']
        grupo.user_set.add(usuario)
        grupo.save()
        signal_usuario_add_grupo_usuarios.send(sender=usuario, usuario=usuario, grupo=grupo, autor=request.user)
        return redirect("usuarios:detalhe_usuario", id_usuario=id_usuario)

    return render(request, 'usuarios/gerenciamento/detalhe_usuario.html', {'usuario': usuario})


@login_required
def exclui_usuario_sistema(request, id_usuario):
    usuario = get_object_or_404(get_user_model(), pk=id_usuario)
    usuario.is_active = False
    usuario.save()
    signal_excluir_usuarios_usuario.send(sender=usuario, usuario=usuario, autor=request.user)
    return redirect("usuarios:lista_usuarios")


@login_required
def restaura_usuario_sistema(request, id_usuario):
    if not check_perm(request.user, permissoes='change_usuario'):
        return HttpResponseForbidden()
    usuario = get_object_or_404(get_user_model(), pk=id_usuario)
    usuario.is_active = True
    usuario.save()
    signal_restaurar_usuarios.send(sender=usuario, usuario=usuario, autor=request.user)
    return redirect("usuarios:lista_usuarios")


@login_required
def editar_usuario_sistema(request, id_usuario=None):
    """ Antes de entrar na tela de edição checar se
        o usuário que solicita é o usuário logado. """

    if not id_usuario and check_perm(request.user, 'add_usuario'):
        # Criar um novo usuário
        u_form = AdminUsuarioCreate(request.POST or None, instance=None)
        verb = _(u'criou')
        usuario = None
    elif id_usuario and (check_perm(request.user, 'change_usuario') or str(request.user.id) == id_usuario):
        # Editar usuário existente
        usuario = get_object_or_404(get_user_model(), pk=id_usuario)
        u_form = AdminUsuarioUpdate(request.POST or None, instance=usuario)
        verb = _(u'editou')
    else:
        return HttpResponseForbidden()

    ctx = {'usuario': usuario, 'u_form': u_form}

    if check_perm(request.user, permissoes="change_group") and id_usuario:
        g_form = FormUsuarioGrupo(request.POST or None)
        ctx['g_form'] = g_form

        if g_form.is_valid():
            grupo = g_form.cleaned_data['grupo']
            grupo.user_set.add(usuario)
            grupo.save()
            return redirect("usuarios:detalhe_usuario", id_usuario=id_usuario)

    if (u_form.is_valid() and (check_perm(request.user, permissoes=['add_usuario', 'change_usuario']) or
                               str(request.user.id) == id_usuario)):
        usuario = u_form.save(commit=False)

        if not id_usuario:
            usuario.set_password(None)

        usuario.save()
        signal_editar_usuarios.send(sender=usuario, verb=verb, usuario=usuario, autor=request.user)
        if not id_usuario:
            html = get_template('usuarios/emails/nova_conta.html')
            current_site = request.get_host()
            site_name = current_site
            domain = current_site
            c = Context({'user': usuario,
                         'domain': domain,
                         'protocol': 'http',
                         'email': usuario.email,
                         'site_name': site_name,
                         'uid': urlsafe_base64_encode(force_bytes(usuario.pk)),
                         'token': default_token_generator.make_token(usuario)})
            html = html.render(c)
            usuario.email_user(_("Nova conta criada"), _("Nova conta criada"), html_message=html)
            messages.success(request, _(u"Usuário {usuario} cadastrado com sucesso.").format(**{'usuario': usuario}))

        return redirect("usuarios:detalhe_usuario", id_usuario=usuario.id)

    return render(request, 'usuarios/gerenciamento/cria_usuario.html', ctx)


def esqueci_senha(request):
    form = EsqueciSenha(request.POST or None)

    if form.is_valid():
        email = form.cleaned_data['email']
        try:
            usuario = get_user_model().objects.get(email=email)
        except:
            return redirect('/')

        # enviar email para o usuário setar a senha
        html = get_template('usuarios/emails/nova_conta.html')

        current_site = get_current_site(request)
        site_name = current_site.name
        domain = current_site.domain

        c = Context({
            'email': usuario.email,
            'domain': domain,
            'site_name': site_name,
            'uid': urlsafe_base64_encode(force_bytes(usuario.pk)),
            'user': usuario,
            'token': default_token_generator.make_token(usuario),
            'protocol': 'http',
        })

        html = html.render(c)

        usuario.email_user("Redefinir senha", "Redefinir senha", html_message=html)

        return redirect('/')

    else:
        return render(request, 'usuarios/gerenciamento/esqueci_senha.html', {'form': form})


def reseta_senha(request, uidb64, token):

    resposta = password_reset_confirm(request,
                                      uidb64=uidb64,
                                      token=token,
                                      template_name='usuarios/gerenciamento/password_reset_confirm.html',
                                      post_reset_redirect='/')
    return request.POST and resposta or resposta.render()


@login_required
def troca_senha(request):
    id_usuario = request.user.id
    resposta = password_change(request,
                               template_name='usuarios/gerenciamento/password_change_form.html',
                               post_change_redirect=reverse("usuarios:detalhe_usuario", args=[id_usuario]))
    if request.POST:
        messages.success(request, u'Sua senha foi atualizada com sucesso!')

    return request.POST and resposta or resposta.render()


@login_required
def detalhe_usuario(request, id_usuario):
    u = get_object_or_404(get_user_model(), pk=id_usuario)
    ctx = {"usuario": u,
           "usuario_logado": request.user}
    return render(request, 'usuarios/gerenciamento/detalhe_usuario.html', ctx)


def _filtra_model(filtros, model):
    if filtros:
        return model.objects.filter(**filtros)
    else:
        return model.objects.all()


@login_required
def busca_usuarios(request, form_busca):
    return _filtra_model(form_busca.extrair_filtro(), get_user_model())
