# coding: utf-8
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _
from .forms import UsuarioCreateForm, UsuarioChangeForm


class UsuarioAdmin(UserAdmin):
    form = UsuarioChangeForm
    add_form = UsuarioCreateForm
    list_display = ("nome_usuario", "nome_completo", "email", "is_staff", "is_active", )
    ordering = ("nome_usuario", )
    fieldsets = (
        (None, {'fields': ('email', 'password', 'nome_completo', 'nome_usuario')}),
        (_('Permissões'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
    )
    add_fieldsets = (
        (
            None,
            {
                'classes': ('wide',),
                'fields': ('nome_usuario',
                           'nome_completo',
                           'email',
                           'is_staff',
                           'is_active')
            }
        ),
    )
