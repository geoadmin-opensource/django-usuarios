# django-usuarios: Models

Dois modelos são oferecidos no **django-usuarios**.  
___


### UsuarioManager(BaseUserManager)

* Modelo herda de BaseUserManager de django.contrib.auth.models
* Sobrescreve os métodos `create_user()` e  `create_superuser()`



### UsuarioBase(AbstractBaseUser, PermissionsMixin)

* Modelo abstrato que herda de AbstractBaseUser.
* Possui os seguintes fields:
	* data_hora_criaçao (*model.DateTimeField*)
	* nome_usuario (*models.CharField*) (*unique=True*)
   	* nome_completo (*models.CharField*)
	* email (*models.EmailField*) (*unique=True*)
	* is_staff (*models.BooleanField*)
	* is_active (*models.BooleanField*)
	* ultimo_login (*models.DateTimeField*)


* Implementa os seguintes métodos:

### 1. `__unicode__`(self)
	retorna o nome_usuario
### 2. `get_full_name`(self)
	retorna o nome_completo
### 3. `get_short_name`(self)
 	retorna o nome_usuario
### 4. `email_user`(self, subject, message, from_email=None, html_message=None)
	metódo responsável por disparar um send_mail


# Como Usar

Para usar basta importar o model **UsuarioBase**, com:
```python
from usuarios.models import UsuarioBase
```
Após importar, crie uma classe qualquer herdando UsuarioBase e aplique as modificações necessárias de acordo com as necessidades do projeto.
