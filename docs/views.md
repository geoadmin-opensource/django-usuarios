# django-usuarios: Views  

O `django-usuarios` fornece um conjunto de views para praticamente todas as funções relacionadas a gerenciamento de usuários, grupos e permissões.  
___


### 1. `check_perm`(usuario, permissoes)
 	responsável por checar as permissões de um determinado usuário.  

### 2. `login`(request)
 	view personalizada para lidar com o login, o comando padrão 'login' do django é importado como 'login_django'.  

### 3. `logout`(request)
	view personalizada para lidar com logout, importa o comando padrão 'logout' do django como 'logout_django' e o dispara, retornando um redirect para view de login.  

### 4. `confirm_exclui_usuario`(request, id_usuario)
	lida com a confirmação de exclusão de um determinado usuário.  

### 5. `confirm_exclui_usuario_grupo`(request, id_grupo, id_usuario)
	lida com a confirmação de exclusão de um determinado usuário em um determinado grupo.  

### 6.  `confirm_exclui_permissao_grupo`(request, id_grupo, id_permissao)
	 lida com a confirmação de exclusão de determinada permissão em um determinado grupo.  

### 7. `confirm_exclui_grupo`(request, id_grupo)
	lida com a confirmação de exclusão de determinado grupo.  

### 8. `lista_grupos`(request)
	lida com a listagem de grupos.  

### 9. `busca_grupos`(request, form_busca)
	lida com a busca de grupos.  

### 10. `criar_grupo`(request)
 	lida com a criação de grupos.  

### 11. `detalhe_grupo`(request, id_grupo)
	lida com a renderização de uma página de detalhes de determinado grupo definido pelo id_grupo passado como parâmetro.  

### 12. `busca_permissoes`(request)
	lida com a busca de permissões, retorna lista json.  

### 13. `busca_usuarios_json`(request)
 	lida com a busca de usuários, retorna lista json.  

### 14. `add_usuarios_grupo`(request, id_grupo)
	lida com a adição de usuários a determinado grupo.  

### 15. `exclui_usuario_grupo`(request, origem, id_grupo, id_usuario)
	lida com a exclusão de um determinado usuário em um determinado grupo.  

### 16. `add_permissoes`(request, id_grupo)
	lida com a adição de permissões a um determinado grupo de permissões.  

### 17. `exclui_permissao`(request, id_grupo, id_permissao)
	lida com a exclusão de determinada permissão em um determinado grupo.  

### 18. `exclui_grupo`(request, id_grupo)
	lida com a exclusão de determinado grupo.  

### 19. `lista_usuarios`(request)
	lida com a listagem de usuários.  

### 20. `adiciona_usuario_grupo`(request, id_usuario)
 	lida com a adição de determinado usuário a um grupo.   

### 21. `exclui_usuario_sistema`(request, id_usuario)
 	lida com a exclusão de um determinado usuário do sistema.  

### 22. `restaura_usuario_sistema`(request, id_usuario)
	desfaz a exclusão de um determinado usuário do sistema.  

### 23. `editar_usuario_sistema`(request, id_usuario=None)
	lida com a edição de um usuário no sistema.  

### 24. `esqueci_senha`(request)
	view responsável por renderizar um formulário de esquecimento de senha.  

### 25. `reseta_senha`(request, uidb64, token)
	view responsável por resetar a senha.  

### 26. `troca_senha`(request)
 	view responsável pela troca de senha.  

### 27. `detalhe_usuario`(request, id_usuario)
	lida com a renderização de uma página de detalhes de determinado usuário definido pelo id_usuario passado como parâmetro.  

### 28. `busca_usuarios`(request, form_busca)
	lida com a busca de usuários do sistema.  
