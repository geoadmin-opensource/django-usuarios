# django-usuarios

Aplicação padrão da SIGMA para criação e autenticação de usuários, troca de senhas, etc.

# Instalação

Primeiramente, instale o pacote **django-usuarios** na virtualenv:

```
pip install -r -e git+http://gitlab.sigmageosistemas.com.br/dev/django-usuarios.git@master#egg=usuarios
```
Agora, adicione o app usuarios no **INSTALLED_APPS**:
```python
INSTALLED_APPS = [
    ...
    'usuarios',
]
```
Configure a **URL**:
```python
urlpatterns = [
	...
    url(r'^usuarios/',  include('usuarios.urls', namespace="usuarios")),
]

```  

_______________________________________________________________

# Funcionalidades

* [Models](docs/models.md)
    * [UsuarioManager](docs/models.md#usuariomanagerbaseusermanager)
    * [UsuarioBase](docs/models.md#usuariobaseabstractbaseuser-permissionsmixin)
* [Views](docs/views.md)
    * [check_perm](docs/views.md#1-check_permusuario-permissoes)
    * [login](docs/views.md#2-loginrequest)
    * [logout](docs/views.md#3-logoutrequest)
    * [confirm_exclui_usuario](docs/views.md#4-confirm_exclui_usuariorequest-id_usuario)
    * [confirm_exclui_usuario_grupo](docs/views.md#5-confirm_exclui_usuario_gruporequest-id_grupo-id_usuario)
    * [confirm_exclui_permissao_grupo](docs/views.md#6-confirm_exclui_permissao_gruporequest-id_grupo-id_permissao)
    * [confirm_exclui_grupo](docs/views.md#7-confirm_exclui_gruporequest-id_grupo)
    * [lista_grupos](docs/views.md#8-lista_gruposrequest)
    * [busca_grupos](docs/views.md#9-busca_gruposrequest-form_busca)
    * [criar_grupo](docs/views.md#10-criar_gruporequest)
    * [detalhe_grupo](docs/views.md#11-detalhe_gruporequest-id_grupo)
    * [busca_permissoes](docs/views.md#12-busca_permissoesrequest)
    * [busca_usuarios_json](docs/views.md#13-busca_usuarios_jsonrequest)
    * [add_usuarios_grupo](docs/views.md#14-add_usuarios_gruporequest-id_grupo)
    * [exclui_usuario_grupo](docs/views.md#15-exclui_usuario_gruporequest-origem-id_grupo-id_usuario)
    * [add_permissoes](docs/views.md#16-add_permissoesrequest-id_grupo)
    * [exclui_permissao](docs/views.md#17-exclui_permissaorequest-id_grupo-id_permissao)
    * [exclui_grupo](docs/views.md#18-exclui_gruporequest-id_grupo)
    * [lista_usuarios](docs/views.md#19-lista_usuariosrequest)
    * [adiciona_usuario_grupo](docs/views.md#20-adiciona_usuario_gruporequest-id_usuario)
    * [exclui_usuario_sistema](docs/views.md#21-exclui_usuario_sistemarequest-id_usuario)
    * [restaura_usuario_sistema](docs/views.md#22-restaura_usuario_sistemarequest-id_usuario)
    * [editar_usuario_sistema](docs/views.md#23-editar_usuario_sistemarequest-id_usuarionone)
    * [esqueci_senha](docs/views.md#24-esqueci_senharequest)
    * [reseta_senha](docs/views.md#25-reseta_senharequest-uidb64-token)
    * [troca_senha](docs/views.md#26-troca_senharequest)
    * [detalhe_usuario](docs/views.md#27-detalhe_usuariorequest-id_usuario)
    * [busca_usuarios](docs/views.md#28-busca_usuariosrequest-form_busca)

# RoadMap

a definir
